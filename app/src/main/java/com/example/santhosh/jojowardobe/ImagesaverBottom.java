package com.example.santhosh.jojowardobe;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by santhosh on 6/28/16.
 */
public class ImagesaverBottom extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "bottomwear.db";
    private static final String TOPWEAR_TABLE_NAME ="bottomwear" ;
    private static final String TOPWEAR_COLUMN_NAME = "path";


    public ImagesaverBottom(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(
                "create table  "+ TOPWEAR_TABLE_NAME+
                        "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"+TOPWEAR_COLUMN_NAME+" text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TOPWEAR_TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public boolean insertImage(String ImagePath ){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TOPWEAR_COLUMN_NAME,ImagePath);
        long retid = db.insert(TOPWEAR_TABLE_NAME,null,contentValues);
        return true;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TOPWEAR_TABLE_NAME);
        return numRows;
    }


    public ArrayList<String> getAllimages()
    {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TOPWEAR_TABLE_NAME, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex(TOPWEAR_COLUMN_NAME)));
            res.moveToNext();
        }
        return array_list;
    }

}
