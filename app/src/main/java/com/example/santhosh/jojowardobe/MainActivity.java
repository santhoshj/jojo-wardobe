package com.example.santhosh.jojowardobe;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity implements ImageFragment.OnFragmentInteractionListener {


    private static final int PICK_IMAGE_ID = 123;
    private myTopAdapter mTopAdapter;
    private myBottomAdapter mBottomAdapter;
    private Animation rotate_fab;
    private Imagesaver myTopdb;
    private ImagesaverBottom myBottomdb;
    private favoritesaver mfavdb;
    private static ArrayList<String> Topimages, BottomImages;


    private int flag = -1;
    private ViewPager viewPagerTopWear;
    private ViewPager viewPagerBottomWear;
    private int topselected = 0;
    private int bottomselected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myTopdb = new Imagesaver(this);
        Topimages = myTopdb.getAllimages();

        myBottomdb = new ImagesaverBottom(this);
        BottomImages = myBottomdb.getAllimages();

        mfavdb = new favoritesaver(this);
        rotate_fab = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_fab);


        FloatingActionButton add_actionButton = (FloatingActionButton) findViewById(R.id.fab1);
        add_actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 1;
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(getApplication());
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
            }
        });


        FloatingActionButton add_actionButton2 = (FloatingActionButton) findViewById(R.id.fab2);
        add_actionButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 0;
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(getApplication());
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
            }
        });


        final FloatingActionButton fav_actionButton = (FloatingActionButton) findViewById(R.id.fab3);
        fav_actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String image1 = Topimages.get(topselected);
                    String image2 = BottomImages.get(bottomselected);
                    Log.d("image path", " " + image1 + "\n " + image2);

                    mfavdb.insertImage(image1, image2);
                    fav_actionButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_favorite_black_24dp));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        fav_actionButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Intent it = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(it);

                return false;
            }
        });

        final FloatingActionButton refresh_actionButton = (FloatingActionButton) findViewById(R.id.fab4);
        refresh_actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refresh_actionButton.startAnimation(rotate_fab);
                fav_actionButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_favorite_border_black_24dp));
                refreshImages();

            }
        });


        viewPagerTopWear = (ViewPager) findViewById(R.id.view);
        viewPagerBottomWear = (ViewPager) findViewById(R.id.view1);

        viewPagerTopWear.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                topselected = position;
                fav_actionButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_favorite_border_black_24dp));

            }

            @Override
            public void onPageSelected(int position) {
                topselected = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }


        });
        viewPagerBottomWear.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                fav_actionButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_favorite_border_black_24dp));
                bottomselected = position;
            }

            @Override
            public void onPageSelected(int position) {
                bottomselected = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mTopAdapter = new myTopAdapter(getSupportFragmentManager());
        viewPagerTopWear.setAdapter(mTopAdapter);

        mBottomAdapter = new myBottomAdapter(getSupportFragmentManager());
        viewPagerBottomWear.setAdapter(mBottomAdapter);

        try {
            refreshImages();
            setAlarm();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void refreshImages() {
        Random rand = new Random();
        if(viewPagerTopWear==null)
            return;
        if (viewPagerBottomWear==null)
            return;
        int Topmax = viewPagerTopWear.getAdapter().getCount();
        int bottommax = viewPagerBottomWear.getAdapter().getCount();
        if (Topmax > 1) {
            int topold = viewPagerTopWear.getCurrentItem();
            int topnew = topold;
            if (Topmax != 1)
                while (topold == topnew)
                    topnew = rand.nextInt(Topmax);
            viewPagerTopWear.setCurrentItem(topnew, true);
        }


        if (bottommax > 1) {
            int bottomold = viewPagerBottomWear.getCurrentItem();
            int bottomnew = bottomold;
            if (bottommax != 1)
                while (bottomold == bottomnew)
                    bottomnew = rand.nextInt(bottommax);
            viewPagerBottomWear.setCurrentItem(bottomnew, true);
        }
    }

    private String saveToInternalStorage(Bitmap bitmapImage) throws IOException {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
        String currentTimeStamp = dateFormat.format(new Date());
        File mypath = new File(directory, currentTimeStamp + ".jpg");
        String retPath = null;
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);

            retPath = mypath.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fos.close();
        }
        return retPath;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("activity result ", "request code :" + requestCode + "result code :" + resultCode);

        if (resultCode == RESULT_OK)
            switch (requestCode) {
                case PICK_IMAGE_ID:
                    Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
                    try {
                        String path = saveToInternalStorage(bitmap);
                        if (path != null) {
                            if (flag == 0) {
                                myTopdb.insertImage(path);
                                Topimages = myTopdb.getAllimages();
                                mTopAdapter.notifyDataSetChanged();

                                Log.i("rowcount top", myTopdb.numberOfRows() + " ");
                                Log.i("rowcount top", myTopdb.getAllimages() + " ");

                            } else {
                                myBottomdb.insertImage(path);
                                BottomImages = myBottomdb.getAllimages();
                                mBottomAdapter.notifyDataSetChanged();

                                Log.i("rowcount bottom ", myBottomdb.numberOfRows() + " ");
                                Log.i("rowcount bottom", myBottomdb.getAllimages() + " ");

                            }
                            Toast.makeText(getApplicationContext(), "image added", Toast.LENGTH_SHORT).show();
                        }
                        else
                        Toast.makeText(getApplicationContext(), "image not added", Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    super.onActivityResult(requestCode, resultCode, data);
                    break;

            }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    public static class myTopAdapter extends FragmentStatePagerAdapter {

        public myTopAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return ImageFragment.newInstance("", Topimages.get(position));
        }

        @Override
        public int getCount() {
            return Topimages.size();
        }
    }

    public static class myBottomAdapter extends FragmentStatePagerAdapter {

        public myBottomAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ImageFragment.newInstance("", BottomImages.get(position));
        }


        @Override
        public int getCount() {
            return BottomImages.size();
        }
    }


    private void setAlarm() {

        Intent intent = new Intent(MainActivity.this, MyReceiver.class);

        int REQUEST_CODE = 2500;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, intent, 0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        // Set the alarm to start at approximately 6:00 a.m.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 6);


        // With setInexactRepeating(), you have to use one of the AlarmManager interval
        // constants--in this case, AlarmManager.INTERVAL_DAY.calendar.getTimeInMillis()
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 5000,
                AlarmManager.INTERVAL_DAY, pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Log.d("next alarm :", " " + alarmManager.getNextAlarmClock().getTriggerTime());

        }
        // alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + alarm_time , pendingIntent);
        //System.out.println("Time Total ----- "+(System.currentTimeMillis()+total_mili));


    }

}
