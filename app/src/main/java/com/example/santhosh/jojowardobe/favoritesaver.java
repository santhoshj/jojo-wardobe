package com.example.santhosh.jojowardobe;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by santhosh on 6/28/16.
 */
public class favoritesaver extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "favorites.db";
    private static final String TABLE_NAME ="favorites" ;
    private static final String TOPWEAR_COLUMN_NAME = "path1";
    private static final String BOTTOMWEAR_COLUMN_NAME = "path2";
    Context context;

    public favoritesaver(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(
                "create table  "+ TABLE_NAME+
                        "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"+TOPWEAR_COLUMN_NAME+" text,"+BOTTOMWEAR_COLUMN_NAME+" text,UNIQUE("+TOPWEAR_COLUMN_NAME+", "+BOTTOMWEAR_COLUMN_NAME+") ON CONFLICT FAIL)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public boolean insertImage(String ImagePath1,String ImagePath2 ){


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TOPWEAR_COLUMN_NAME,ImagePath1);
        contentValues.put(BOTTOMWEAR_COLUMN_NAME,ImagePath2);
        //long retid = db.insert(TABLE_NAME,null,contentValues);
        try {
            db.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_FAIL);
        }catch (Exception e){

            Toast.makeText(context,"already exists",Toast.LENGTH_SHORT).show();
            Log.d("failed","inthe catch\n");
            e.printStackTrace();
        }

        return true;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        return numRows;
    }



    public ArrayList<dressPair> getAllimages()
    {
        ArrayList<dressPair> array_list = new ArrayList<>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_NAME, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            String top=   (res.getString(res.getColumnIndex(TOPWEAR_COLUMN_NAME)));
            String bottom=   (res.getString(res.getColumnIndex(BOTTOMWEAR_COLUMN_NAME)));
            array_list.add(new dressPair(top,bottom));
            res.moveToNext();
        }
        return array_list;
    }

}
