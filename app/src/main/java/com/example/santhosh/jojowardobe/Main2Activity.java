package com.example.santhosh.jojowardobe;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity implements favoritesFragment.OnFragmentInteractionListener {

    private favoritesaver mfavdb;
    private static ArrayList<dressPair> dressPairs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mfavdb= new favoritesaver(this);
        dressPairs =mfavdb.getAllimages();

        ViewPager mViewPager= (ViewPager) findViewById(R.id.view3);
        myfavAdapter adapter= new myfavAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);


    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }





    public static class myfavAdapter extends FragmentStatePagerAdapter {

        public myfavAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return favoritesFragment.newInstance(dressPairs.get(position).getTop(),dressPairs.get(position).Bottom);
        }

        @Override
        public int getCount() {
            return dressPairs.size();
        }
    }


}
