package com.example.santhosh.jojowardobe;

/**
 * Created by santhosh on 6/28/16.
 */
public class dressPair {
    String Top;
    String Bottom;

    public dressPair(String top, String bottom) {
        Top = top;
        Bottom = bottom;
    }

    public String getTop() {
        return Top;
    }

    public void setTop(String top) {
        Top = top;
    }

    public String getBottom() {
        return Bottom;
    }

    public void setBottom(String bottom) {
        Bottom = bottom;
    }
}
